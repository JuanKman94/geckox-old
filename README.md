# Geckox
Repository for my personal website

Used to have a Dockerfile but I realized it was just reinventing the wheel
(for a static website configuration, of all things...). So now, to build the
website pull the official jekyll docker image and use it to build it or for
testing, whatever. Then publish it however you like to wherever you like

```bash
$ docker pull jekyll/jekyll
$ docker run \
    -it \
    --rm \
    --name geckox \
    -v ${PWD}:/srv/jekyll \
    -p 4000:4000 \
    jekyll/jekyll jekyll serve
```
