---
title: "Estudio de caso: Capacitated Facility Location Problem"
classname: "Temas Selectos de Optimización"
abstract: >
    Como proyecto integrador de la materia se realiza un estudio a el
    problema de localización de plantas con capacidad limitada. Se
    realiza una investigación del problema, se implementa una heurística
    constructiva y después una de búsqueda local para mejores soluciones.
math: true
date: 2017-05-16
authors:
    - Juan Carlos Guajardo García &mdash; 1516330
references:
    - id: miranda_2005
      title: >
        Miranda y Garrindo, 2005. Modelo de inventario y localización de
        instalaciones con restricciones estocásticas de capacidad de
        inventario resuelto mediante relajación lagrangeana.
      href: "http://www.sochitran.cl/wp-content/uploads/Acta-2005-05-01.pdf"
    - id: marulanda_2010
      title: >
        Marulanda, Leguizámon y Niño, 2010. Solución al problema de
        localización (CFLP) a través de búsqueda tabú y relajación
        lagrangeana, caso de estudio: industria de productos alimentarios
---
<h1 hidden>Reporte</h1>
<article>
    <h2>Resumen</h2>
    <p>La toma decisiones para colocación de plantas en un plano es un
    problema con amplio alcance para búsqueda de soluciones. En este
    estudio se analiza el modelo de Localización de Plantas con
    restricción de Capacidad, que es una variedad del problema de
    localización de plantas con la condición extra de que las plantas
    tienen una capacidad individual máxima de producción, requiriendo
    así una mayor consideración en la selección de localizaciones.
</article>

<article>
    <h2>1 Introducción</h2>
    <p>El problema consiste, en grandes rasgos, en encontrar una
    distribución en un plano buscando satisfacer las necesidades de todos
    los clientes de éstas optimizando, a su vez, alguna variable de toma
    de decisión, sea maximizar la cantidad de clientes por planta,
    minimizar costos de construcción y mantenimiento de plantas, etc.

    <p>En este estudio se realiza una heurística constructiva que, dada
    una matriz de capacidades de las plantas y una matriz de costos de
    transporte de cada planta a cada cliente, se ordenan las plantas
    de manera ascendente acorde a la sumatoria de todos sus costos de
    transporte. Una vez ordenadas las plantas, se busca abastecer a la
    mayor cantidad de clientes hasta que su capacidad lo permita.
    Esto se discute mas a fondo en la sección 2.

    <p>Después, como se muestra en la sección 3, se aplica la Relajación
    Lagrangeana a la cota obtenida por la heurística para buscar una
    solución mejor al problema. Los resultados obtenidos se muestran en
    la sección 4 y se exponen las conclusiones en la sección 5.
</article>

<article>
    <h2>2 Formulación del problema</h2>
    <p>En el modelo estudiado se cuenta con una cantidad finita de plantas
    cuya capacidad de producción total, i.e., sumando la de todas las
    plantas, es igual a la demanda total de los clientes, por lo que la
    solución del problema yace en obtener la distribución de menor costo
    desde las plantas hacia los clientes, de modo que hay que determinar
    qué plantas deben mantenerse abiertas y restringiendo la capacidad
    de proveer a un cliente desde más de una planta.

    <p>Si consideramos a <var>n</var> como el número total de clientes en
    el área; <var>m</var> como el número de plantas;
    <var>C<sub>ij</sub></var>, con <var>I = { 1, 2, ..., n }</var> y 
    <var>J = { 1, 2, ..., m }</var>, el costo de atender la demanda del
    cliente <var>i</var> desde la planta <var>j</var>;
    <var>d<sub>i</sub></var> la demanda del cliente <var>i</var>;
    <var>S<sub>j</sub></var> la capacidad de la planta <var>j</var>;
    <var>f<sub>j</sub></var> el costo fijo de la planta <var>j</var>.
    Con las variables <var>Y<sub>j</sub></var> y
    <var>X<sub>ji</sub></var> definidas como:

    <table class=text-center>
        <caption>Variables de decisión del problema</caption>
        <tbody>
            <tr>
                <td>$$
                    X_{ji} = \begin{cases}
                        1 &amp; \quad \text{el cliente i es atendido por la planta j}\\
                        0 &amp; \quad \text{no es así}\\
                    \end{cases}
                $$
                <td>$$
                    Y_{i} = \begin{cases}
                        1 &amp; \quad \text{la planta j está operativa}\\
                        0 &amp; \quad \text{no es así}\\
                    \end{cases}
                $$
        </tbody>
    </table>

    <p>La demanda de los clientes, las capacidades de las plantas y los
    costos fijos de las mismas, se conocen de antemano. El objetivo es
    encontrar la ruta de distribución de plantas a clientes con menor
    costo.

    <h3 class="break-before">2.1 Formulación matemática</h3>

    <table class="text-center">
        <tbody>
            <tr>
                <td>$$
                        min \sum_{i}^n
                        \sum_{j}^m C_{ij}X_{ji}
                        + \sum_{j}^m f_j Y_j
                    $$
                <td>(1)
            <tr>
                <td>$$ \sum_{j}^m X_{ji} = 1, &forall; i $$
                <td>(2)
            <tr>
                <td>$$ \sum_{i}^n d_{i} X_{ji} &le; S_j Y_j, &forall; j $$
                <td>(3)
            <tr>
                <td>$$ \sum_{j}^m S_{j} Y_{j} &ge; \sum_{i}^n d_i $$
                <td>(4)
            <tr>
                <td><var>Y<sub>j</sub></var> = { 0, 1 }
                <td>(5)
            <tr>
                <td><var>X<sub>ji</sub></var> = { 0, 1 }
                <td>(6)
        </tbody>
    </table>

    <p>La función objetivo (1) es la suma de los costos totales de
    distribución más los costos anuales de mantenimiento de las plantas.
    La restricción (2) establece que cada cliente sea abastecido por
    máximo una sola planta; la (3) garantiza que toda planta abierta tenga
    la capacidad de atender todas las demandas asignadas, en otras palabras,
    que las plantas pueden abastecer a sus clientes; la restricción
    (4) que la demanda total de la zona sea cubierta por las plantas
    abiertas; las restricciones (5) y (6) mantienen a nuestras variables
    de decisión como binarias.

    <h3>2.2 Heurísticas</h3>
    <p>Las <em>heurísticas constructivas</em> diseñadas consisten en
    ordenar la secuencia en que son utilizadas las plantas. Por ejemplo,
    ordenar las plantas de menor a mayor costo fijo de operaciones.

    <h4>2.2.1 Heurística 1</h4>
    <p>Se ordenan las plantas de manera ascendente por su costo fijo de
    operaciones y, una vez ordenadas, se revisa cuáles clientes pueden
    ser atendidos por ellas.
    <ul class="pseudo-code">
        <li>I = { 1, 2, ..., n }; // clientes
        <li>J = { 1, 2, ..., m }; // plantas
        <li>f = {}; // costos fijos
        <li>Ordenar J de menor a mayor costo fijo
            <var>f<sub>j</sub></var>, &forall; j
        <li>FOR j &isin; J
            <ul>
                <li>FOR i &isin; I
                <ul>
                    <li>IF $ d_{i} + \sum_{i}^n d_{i} X_{ji} &le; S_j $
                        // j tiene suficiente capacidad para abastecer a i
                    <ul>
                        <li>Y<sub>j</sub> &larr; 1
                        <li>X<sub>ji</sub> &larr; 1
                        <li>I &larr; I \ {i}
                    </ul>
                    <li>ENDIF
                </ul>
            </ul>
        <li>RETURN X, Y
    </ul>

    <h4>2.2.2 Heurística 2</h4>
    <p>Se ordenan las plantas de manera ascendente por la suma de sus
    costos de transporte a los clientes operaciones y, una vez ordenadas,
    se revisa cuáles clientes pueden ser atendidos por ellas.
    <ul class="pseudo-code">
        <li>I = { 1, 2, ..., n }; // clientes
        <li>J = { 1, 2, ..., m }; // plantas
        <li>C = {}; // costos de transporte
        <li>Ordenar J de menor a mayor costo total de transporte
            <var>C<sub>ij</sub></var>, &forall; i &forall; j
        <li>FOR j &isin; J
            <ul>
                <li>FOR i &isin; I
                <ul>
                    <li>IF $ d_{i} + \sum_{i}^n d_{i} X_{ji} &le; S_j $
                        // j tiene suficiente capacidad para abastecer a i
                    <ul>
                        <li>Y<sub>j</sub> &larr; 1
                        <li>X<sub>ji</sub> &larr; 1
                        <li>I &larr; I \ {i}
                    </ul>
                    <li>ENDIF
                </ul>
            </ul>
        <li>RETURN X, Y
    </ul>

    <h3>2.3 Resultados</h3>
    <figure style="margin: 1rem auto; max-width: initial;">
        <canvas id=low_50_100_10000 style="max-width: 100%;"></canvas>
        <figcaption>
            <b>Figura 1</b>. Comparación entre las heurísticas con instancias
            de <b>50</b> plantas con capacidad fija de <b>10,000</b> y
            <b>100</b> clientes con costos de transporte entre <b>75</b> y
            <b>100</b> unidades.
        </figcaption>
    </figure>

    <figure style="margin: 1rem auto; max-width: initial;">
        <canvas id=high_50_100_10000 style="max-width: 100%;"></canvas>
        <figcaption>
            <b>Figura 2</b>. Comparación entre las heurísticas con instancias
            de <b>50</b> plantas con capacidad fija de <b>10,000</b> y
            <b>100</b> clientes con costos de transporte entre <b>500</b> y
            <b>750</b> unidades.
        </figcaption>
    </figure>

    <h2>3 Antecedentes</h2>
    <p>La localización de plantas para el abastecimiento de clientes es un
    tema recurrente en el área de investigación de operaciones, explorando
    también procesos meta heurísticos para la mejora de soluciones de
    heurísticas constructivas (Miranda, 2005), (Marulanda et al., 2010).

    <p>La <strong>búsqueda tabú</strong> es método meta heurístico que
    utiliza memoria adaptativa combinada con estrategias especiales para la
    obtención de soluciones factibles a partir de una cota obtenida
    previamente. Consiste en generar un <em>vecindario</em> de soluciones,
    evaluarlas y encontrar la mejor de ellas. Si se encuentra una mejor
    solución a la original (a esto se le conoce como criterio de aspiración),
    se actualiza como la mejor solución y se continúa buscando a partir de
    ésta nueva. La búsqueda es interrumpida cuando no se alcanza un mejor
    criterio de aspiración después de un número determinado de búsquedas en
    vecindarios generados.

    <h2>4 Metodología propuesta</h2>
    <p>Para buscar en soluciones factibles a partir de una cota se propone
    reajustar los clientes asignados a las plantas operativas y/o intercambiar
    los clientes asignados de una planta <var>x</var> y una planta
    <var>y</var>. El pseudo-código para esto es así:

    <ul class="pseudo-code">
        <li>N = {} // vecindario
        <li>J = { 1, 2, ..., m }; // plantas
        <li><var>Y<sub>j</sub></var> = { 0, 1 }, &forall; j
        <li>FOR x &isin; J
            <ul>
                <li>FOR y &isin; J
                <ul>
                    <li>IF x != y
                    <ul>
                        <li>Y<sub>y</sub> &larr; 0
                        <li>Y<sub>x</sub> &larr; 1
                        <li>N &larr; N + Y
                    </ul>
                    <li>ENDIF
                </ul>
            </ul>
        <li>RETURN N
    </ul>

    <p>También se puede obtener otra variedad de soluciones cuando se
    intercambian los clientes asignados entra las plantas <var>x</var> y
    <var>y</var>.

    <ul class="pseudo-code">
        <li>N = {} // vecindario
        <li>I = { 1, 2, ..., n }; // clientes
        <li>J = { 1, 2, ..., m }; // plantas
        <li><var>Y<sub>j</sub></var> = { 0, 1 }, &forall; j
        <li><var>X<sub>ji</sub></var> = { 0, 1 }, &forall; j, i
        <li>FOR x &isin; J
            <ul>
                <li>FOR y &isin; J
                <ul>
                    <li>IF x != y AND Y<sub>x</sub> == 1 AND Y<sub>y</sub> == 1
                    <ul>
                        <li>tmp &larr; X<sub>x</sub>
                        <li>X<sub>x</sub> &larr; X<sub>y</sub>
                        <li>X<sub>y</sub> &larr; tmp
                        <li>N &larr; N + X
                    </ul>
                    <li>ENDIF
                </ul>
            </ul>
        <li>RETURN N
    </ul>

    <h2>5 Conclusiones</h2>
    <p>El experimento me permitió ver que los parámetros de costo fijo de
    mantenimiento tienen mayor influencia sobre el resultado final que los
    costos de transporte de plantas a clientes. Como se puede observar en las
    gráficas de resultados, la heurística 1 se desempeña mejor que la
    heurística 2 en todos los casos.

    <p>Algo que no muestran, sin embargo, es que el desempeño de la
    heurística 2 da buenos resultados cuando los costos de transporte son
    bajos, pero no se puede asumir que eso suceda en situaciones del mundo
    real.

    <p>La implementación de búsqueda tabú no pudo completarse
    satisfactoriamente, por lo que la comparación de resultados queda
    pendiente para futuros trabajos. El mayor impedimento fue la generación
    exitosa de vecindarios verdaderamente tabú para la comparación con la
    cota inicial, pero no dudo de la efectividad del método para la mejora de
    soluciones, como se ha demostrado previamente (Marulanda et al., 2010),
    entre otros.
</article>

<footer class="clearfix">
    {% include references.html %}
</footer>

<script src=/js/chart.min.js></script>
<script>
var chartHeur1 = {};

window.addEventListener('DOMContentLoaded', function(ev) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/js/charts.json');

    xhr.addEventListener('load', loadCharts);
    xhr.addEventListener('error', function(ev) {
        console.error('Response => ', ev);
    });

    xhr.send();
});

function loadCharts(ev) {
    var charts = JSON.parse(ev.target.response);

    for (key in charts) {
        var canvas = document.getElementById(key);

        if (canvas) {
            new Chart(canvas, charts[key]);
        }
    }
}
</script>
