---
title: "Optimización combinatoria"
classname: "Temas Selectos de Optimización"
abstract: >
    Características sobre el problema de la mochila (Knapsack Problem)
    y el problema del agente viajero (Traveling Salesman Problem)
math: true
date: 2017-02-7
references:
    - title: "Wikipedia. Knapsack Problem"
      href: "https://en.wikipedia.org/wiki/Knapsack_problem"
    - title: "Wikipedia. Traveling Salesman Problem"
      href: "https://en.wikipedia.org/wiki/Travelling_salesman_problem"
---
<section>
    <h2>1. El problema de la mochila &mdash; Knapsack problem</h2>

    <img
        src="https://i.ytimg.com/vi/EH6h7WA7sDw/maxresdefault.jpg"
        alt="Aplicación del problema de la mochila en el juego Minecraft"
        style="float: right; max-width: 35%; margin: auto 1rem;"
    >
    <p>La premisa de este problema es la siguiente: dado un conjunto de
    objetos, cada uno con su peso y valor, determinar la cantidad de cada objeto
    a incluir en una colección de tal manera que el peso total sea menor o
    igual a determinado límite y haciendo el valor obtenido lo más grande
    posible.

    <h3>1.1 - Definición</h3>
    <p>Existen varias presentaciones del problema, pero todas comparten la
    restricción de capacidad máxima, i.e., que la sumatoria de los pesos de
    los objetos no debe sobrepasar a la capacidad <var>W</var>. Entre las
    presentaciones que destacan se encuentran:

    <dl>
        <dt>(1) <strong>Decisión binaria</strong> (0-1)</dt>
        <dd>También conocido como el problema de la mochila 0-1, se restringe
        el número $x_i$ de objetos de cada tipo a una cantidad de 0 o 1. Dado
        un conjunto de objetos <var>n</var> numerados de 1 a <var>n</var>,
        cada uno con un peso $w_i$ y un valor $v_i$.
        </dd>

        <dt>
            (2) <strong>Con tope</strong>
            (<abbr title="Bounded Knapsack Problem">BKP</abbr>)
        </dt>
        <dd>Éste no específica que debe haber exactamente 0 o 1 de cada tipo
        de objeto en la mochila, pero restringe la cantidad $x_i$ a un número
        entero no-negativo menor a <var>c</var>.
        </dd>

        <dt>
            (3) <strong>Sin tope</strong>
            (<abbr title="Unbounded Knapsack Problem">UKP</abbr>)
        </dt>
        <dd>Similar a (2) pero no especifica un tope a la cantidad de objetos,
        simplemente requiere que la cantidad $x_i$ sea un entero no-negativo.
        </dd>
    </dl>

    <h3>1.2 - Modelo matemático</h3>
	<table>
		<tbody>
			<tr>
				<td>Maximizar</td>
				<td>$$ \sum_{i=0}^n v_i x_i $$</td>
				<td></td>
			</tr>
			<tr>
				<td>Sujeto a</td>
				<td>$$ \sum_{i=0}^n v_i x_i &lt; W $$</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>$ x_i &#8712; \{0, 1\} $</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td></td>
				<td>$ 0 &#8804; x_i &#8804; c $</td>
				<td>(2)</td>
			</tr>
			<tr>
				<td></td>
				<td>$ x_i &#8805; 0 $</td>
				<td>(3)</td>
			</tr>
		</tbody>
	</table>

    <h4>1.3 - Soluciones</h4>
    <p>Para resolver el problema de la mochila hay varios algoritmos a la mano;
    basados en programación dinámica, ramificación y límite, o una combinación
    de ambos. Aquí se ilustra una con programación dinámica.

    <h5>1.3.1 - Sin tope (3)</h5>
    <p>Si todos los pesos $ (w_1, ..., w_n) $ son enteros no-negativos, el
    problema puede ser resuelto en un tiempo pseudo-polinomial asumiendo que
    todos los pesos son positivos $ (w &gt; 0) $. Por cada $ w &#8804; W $,
    definimos $m[w]$ con el máximo valor que puede ser obtenido con un peso
    total o menor a <var>w</var>, donde <var>m</var> es una matriz de
    soluciones. Entonces $m[W]$ es la solución al problema y $m[w]$ tiene las
    siguientes propiedades:

    <ul>
        <li>$m[0] = 0$</li>
        <li>$m[w] = {max}_{w_i &#8804; w} (v_i + m[w - w_i])$</li>
    </ul>

    <p>Donde $v_i$ es el valor del objeto <var>i</var>. La idea es que la
    solución es igual al valor de un objeto correcto <em>más</em> una
    solución mas pequeña, específicamente una con la capacidad reducida del
    objeto actual.

    <p>Se tabulan resultados desde $m[0]$ hasta $m[W]$, puesto que el cálculo
    de cada $m[w]$ involucra examinar <var>n</var> objetos y hay <var>W</var>
    valores para $m[w]$ a calcular, el tiempo de solución es $O(nW)$.

    <h3>1.4 - Antecedentes</h3>
</section>

<section>
    <h2>2. El problema del agente viajero &mdash;
        Traveling salesman problem
    </h2>

    <figure class="left">
        <img
            src="http://img09.deviantart.net/4629/i/2015/301/c/b/gregor_samsa___the_metamorphosis_by_lucaorlando-d9en9et.jpg"
            alt="One morning, as Gregor Samsa was waking up from anxious dreams,
            he discovered that in bed he changed a monstrous verminous bug."
            style=""
        >
        <figcaption>Gregor Samsa era un vendedor que vivía,
        principalmente, en hoteles baratos y asientos de tren debido a su
        profesión. Este es el personaje principal de la novela
        <q>La metamorfosis</q> de Franz Kafka.
        </figcaption>
    </figure>

    <p>Dada una lista de ciudades y la distancia que existe entre cada par de
    ellas, ¿cuál es la ruta mas corta posible que visita cada ciudad
    exactamente una vez y regresa a la ciudad de origen?

    <p>Ésta es la pregunta que nos plantea el problema. Sabemos que éste es
    un problema <abbr title="Noapolinomial">NP</abbr>-difícil de
    optimización combinacional, importante en la investigación de
    operaciones y la teoría de la computación.

    <h3>2.1 - Definición</h3>
    <p>El problema puede ser modelado como un grafo de peso no-dirigido, de
    tal manera que las ciudades son vértices, los caminos son las aristas del
    grafo, y la distancia del camino es el peso de la arista. Es un problema
    de minificación en el que se inicia en un vértice determinado y se tiene
    que regresar a el después de haber visitado todos los demás vértices
    exactamente una vez.

    <h3>2.2 - Modelo matemático</h3>
    <p>Para formularlo como un problema de programación lineal entera, hay que
    etiquetar a las ciudades con números del 1 a <var>n</var> y definir:

    $$ x_{ij} =
        \begin{cases}
            1 \quad \text{el camino va de la ciudad i a la ciudad j}\\
            0  \quad \text{no es así}
        \end{cases}
    $$

    <p>Para $ i = 1, ..., n $ dejamos $u_i$ ser una variable artificial y sea
    $c_{ij}$ la distancia entre las ciudades <var>i</var> y <var>j</var>.
    Entonces el modelo matemático puede ser escrito de la siguiente forma:
    </p>

    $$ min \sum_{i=0}^n \sum_{j&#8800;i, i=1}^n c_{ij}x_{ij}: $$
    $$ 0 &#8804; x_i &#8804; c,,  i,j = 1,...,n; $$
    $$ u_i &#8712; Z,,  i = 1,...,n; $$
    $$ \sum_{i=1&#44; i&#8800;j}^n x_{ij} = 1,  j = 1,...,n $$
    $$ \sum_{j=1&#44; j&#8800;i}^n x_{ij} = 1,  i = 1,...,n $$
    $$ u_i - u_j + nx_{ij} &#8804; n - 1,  1 &#8804; i &#8800; j &#8804; n $$
    $$  $$

    <h3>2.3 - Soluciones</h3>
    <ul>
        <li>Por fuerza bruta consistiría en calcular todas las permutaciones
            de manera ordenada y escoger la de menor cantidad de saltos
        </li>
        <li>Algoritmos heurísticos o aproximaciones, tales como heurísticas
            constructivas, la mejora iterativa, la mejora aleatoria, entre
            otras.
        </li>
    </ul>

    <h3>2.4 - Antecedentes</h3>
    La forma general de este problema fue estudiada primeramente durante la
    década de 1930 en Vienna y Harvard, notablemente por Karl Menger, quien
    definió el problema, consideró la obvia opción de la fuerza bruta y
    observó la no-optimilidad la heurístia del vecino mas cercano.

    En las décadas de 1950 y 1960 la corporación RAND de Santa Monica ofreció
    premios por el descubrimiento de los pasos para resolver el problema.
    Algunas contribuciones notables, por George Dantzig, Delbert Ray Fulkerson
    y Selmer M. Johnson, expresaron el problema como uno de programación lineal
    entera y desarrollaron el método de avión cortante para su resolución.<br>
    Resolvieron óptimamente una instancia del problema con 49 ciudades y
    probaron que no había una ruta más corta.
    <p>
</section>

<footer class="clearfix">
    {% include references.html %}
</footer>
